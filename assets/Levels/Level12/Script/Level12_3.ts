import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    BEEP,
    DANCE,
    FREEZE,
    SING_LUPIN,
    THROW,
    WOMAN_ANGRY,
    WOMAN_GIGGLE,
    WOMAN_HMP,
    YAY,
}

@ccclass
export default class Level12_3 extends LevelBase {

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-648, -560), "general/walk", "emotion/worry");
        this.lupin.setCompleteListener(null)
        this.lupin.timeScale = 1
        this.lupin.node.scaleX = 1

        this.setOtherSpine(
            this.otherSpine[0],
            cc.v2(90, -416),
            "police/level_12/girl_decor",
            null,
        );

        this.otherSpine[0].timeScale = 1
        this.otherSpine[0].setCompleteListener(null)

        this.otherSpine[1].node.active = false

        this.setOtherSprite(this.otherSprite[0], cc.v2(95, -100.601))
        this.otherSprite[1].node.active = true

        this.showOptionContainer(false)

    }

    setAction(): void {
        cc.Tween.stopAll();

        this.playSound(SOUND.WOMAN_HMP, false, 1)

        tween(this.lupin.node)
            .to(5, {position: cc.v3(300, -560)})
            .call(() => {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_thinking',
                    'emotion/excited',
                )
            })
            .delay(3)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        let count = 0
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_12/sing_a_song2') {
                count++
                if (count === 1) {
                    this.lupin.setAnimation(0, 'level_12/sing_a_song', false)
                    this.lupin.timeScale = 1
                    count = 0
                }
            }

            if (track.animation.name === 'level_12/sing_a_song') {
                this.scheduleOnce(() => {
                    this.otherSpine[0].setAnimation(0, 'police/level_12/girl_fall_in_love', false)
                    this.lupin.setAnimation(0, 'general/win', true)
                    this.lupin.setAnimation(1, 'emotion/happy_2', true)

                    this.playSound(SOUND.WOMAN_GIGGLE, false, 0)
                    this.playSound(SOUND.YAY, false, 0)
                }, 1)
                
            }

            if (track.animation.name === 'general/win') {
                count++
                if (count === 3) {
                    this.showSuccess(this.selected);
                }
            }
        })

        this.scheduleOnce(() => {
            this.lupin.node.position = cc.v3(350, -560)
            this.lupin.setAnimation(0, 'level_12/sing_a_song2', true)
            this.lupin.clearTrack(1)
            this.lupin.timeScale = .75
            this.lupin.node.scaleX = -1

            this.playSound(SOUND.SING_LUPIN, false, 0)
        }, 2)
    }

    runOption2(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/door_open2') {
                this.lupin.setAnimation(0, 'general/stand_nervous', false)
                this.lupin.setAnimation(1, 'emotion/fear_2', true)

                this.otherSprite[1].node.active = false

                this.otherSpine[1].setAnimation(0, 'level_12/button_fall', false)
                this.otherSpine[1].node.active = true

                this.playSound(SOUND.FREEZE, false, .2)
            }
        })

        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'level_12/button_fall') {
                this.scheduleOnce(() => {
                    this.showContinue()
                }, 2)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(1, 'emotion/idle', true)
            this.lupin.setAnimation(0, 'general/door_open2', false)

            this.playSound(SOUND.BEEP, false, 1.5)
        }, 2)

    }

    runOption3(): void {
        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'police/level_12/girl_throw_comb') {
                this.otherSpine[0].setAnimation(0, 'police/level_12/girl_decor', true)

                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/sad')

                cc.audioEngine.stopAllEffects()

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 2)
            }
        })
        tween(this.node)
            .delay(3)
            .call(() => {
                this.lupin.setAnimation(0, 'emotion/idle', true)
                this.lupin.setAnimation(1, 'general/win_1.1', true)

                this.playSound(SOUND.DANCE, true, 0)
            })
            .delay(3)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_12/girl_throw_comb', false)
                this.playSound(SOUND.WOMAN_ANGRY, false, 0)
                this.playSound(SOUND.THROW, false, 1)
            })
            .start()
        
    }
}
