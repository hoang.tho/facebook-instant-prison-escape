import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND{
    ELECTROCUTE,
    ALARM,
    SNIP,
    TRAP
}

@ccclass
export default class Level10_3 extends LevelBase {

    @property(cc.Node)
    alarm: cc.Node = null;

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-885, -500), "general/run", "emotion/worry");
        for (let sprite of this.otherSprite)
        {
            sprite.node.scale = 1;
            sprite.node.active = true;
        }
        this.camera2d[0].position = cc.v3(0, 0);
        this.alarm.opacity = 0;
        this.alarm.getChildByName("bg_alarm").x = -cc.view.getVisibleSize().width;
        this.background.x = 540.5;
    }

    setAction(): void {
        cc.Tween.stopAll();
        var odd = [];
        var even = [];
        this.playSound(SOUND.TRAP, false, 2)
        this.playSound(SOUND.TRAP, false, 3)
        for (let i = 3; i < this.otherSprite.length; ++i)
        {
            i % 2 == 0 ? odd.push(this.otherSprite[i].node) : even.push(this.otherSprite[i].node);
        }
        this.lupin.setMix("general/run", "general/stand_nervous", 0.3);
        this.playSound
        tween(this.lupin.node).by(2, {position: cc.v3(700, 0)})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.lupin.setAnimation(1, "emotion/fear_2", true);
            }).start();
        tween(this.camera2d[0]).by(1.5, {position: cc.v3(600, 0)}, {easing: "cubicIn"})
            .call(() => {
                tween(this.node).delay(2).call(() => {
                    this.showOptionContainer(true);
                }).start();
            }).start();
        tween(this.node).call(() => {
                for (let arrow of odd)
                {
                    tween(arrow).repeatForever(
                            tween(arrow).to(0.7, {scaleY: 0}).to(0.7, {scaleY: 1}).start()
                        ).start();
                }
            })
            .delay(0.7)
            .call(() => {
                for (let arrow of even)
                {
                    tween(arrow).repeatForever(
                            tween(arrow).to(0.7, {scaleY: 0}).to(0.7, {scaleY: 1}).start()
                        ).start();
                }
            })
            .start();
    }

    runOption1(): void {
        //Cắt dây đỏ
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                let isTrue = true;
                this.lupin.node.position = cc.v3(-130, -350);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_10/cut_line", false);
                this.playSound(SOUND.SNIP, false, 2.55)
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_10/cut_line" && isTrue)
                    {
                        isTrue = false;
                        this.playSound(SOUND.ELECTROCUTE, false, 0.5)
                        this.otherSprite[0].node.active = false;
                        this.lupin.setAnimation(0, "general/electric_die", true);
                        this.lupin.node.position = cc.v3(this.lupin.node.x, this.lupin.node.y - 350);
                        tween(this.node).delay(2).call(() => {
                            cc.Tween.stopAll();
                            this.showContinue();
                        }).start();
                    }
                });
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        //Cắt dây xanh
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                let isTrue = true;
                this.lupin.node.position = cc.v3(-105, -350);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_10/cut_line", false);
                this.playSound(SOUND.SNIP, false, 2.55)
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_10/cut_line" && isTrue)
                    {
                        isTrue = false;
                        this.otherSprite[1].node.active = false;
                        this.playSound(SOUND.ALARM, false, 0.1)
                        tween(this.alarm).repeat(4,
                                tween(this.alarm).to(0.5, {opacity: 255}).to(0.5, {opacity: 0})
                            ).start();
                        tween(this.lupin.node).delay(1).call(() => {
                            this.lupin.setAnimation(0, "general/back", false);
                            this.lupin.setAnimation(1, "emotion/fear_2", true);
                        })
                        .delay(2)
                        .call(() => {
                            cc.Tween.stopAll();
                            this.showContinue();
                        }).start();
                    }
                });
            })
            .to(0.5, {opacity: 0})
            .start();
    }
    
    runOption3(): void {
        //Cắt dây xanh
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                let isTrue = true;
                this.lupin.node.position = cc.v3(-80, -350);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_10/cut_line", false);
                this.playSound(SOUND.SNIP, false, 2.55)
                this.playSound(SOUND.TRAP, false, 3)
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_10/cut_line" && isTrue)
                    {
                        isTrue = false;
                        cc.Tween.stopAll();
                        this.otherSprite[2].node.active = false;
                        for (let i = 3; i < this.otherSprite.length; ++i)
                        {
                            tween(this.otherSprite[i].node).to(0.5, {scaleY: 0}).start();
                        }
                        tween(this.lupin.node).delay(0.5)
                            .call(() => {
                                this.lupin.setAnimation(0, "general/win_2.1", true);
                                this.lupin.setAnimation(1, "emotion/happy_2", true);
                            })
                            .delay(2)
                            .call(() => {
                                cc.Tween.stopAll();
                                this.onPass();
                            })
                            .start();
                    }
                });
            })
            .to(0.5, {opacity: 0})
            .start();
    }
}
