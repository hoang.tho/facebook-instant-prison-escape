import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND{
    BARK,
    BARK_RUN,
    ELECTROCUTE,
    ALERT
}
@ccclass
export default class Level10_1 extends LevelBase {

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1220, -500), "general/stand_thinking", "emotion/worry");
        this.setOtherSpine(this.otherSpine[0], cc.v2(-1250, 390), "police/general/walk", null);
        this.otherSpine[0].node.scaleX = -1;
        this.lupin.node.scaleX = 1;
        this.background.position = cc.v3(1080, 0);
        this.camera2d[0].position = cc.v3(0, 0);
        this.otherSpine[2].node.active = false;
        this.otherSprite[0].node.active = false;
        this.otherSprite[0].node.scale = 2;
        this.otherSprite[1].node.active = false;
        this.otherSpine[2].node.active = false;
        this.otherSpine[1].setAnimation(0, "run_1", true);
    }

    setAction(): void {
        cc.Tween.stopAll();
        this.playSound(SOUND.BARK_RUN, false, 0)
        tween(this.camera2d[0]).delay(1).by(3, {position: cc.v3(2100, 0)})
            .delay(1)
            .by(2, {position: cc.v3(-2100, 0)}, {easing: "cubicOut"})
            .call(() => {
                tween(this.node).delay(1).call(() => {
                    this.showOptionContainer(true);
                }).start();
            })
            .start();
        tween(this.otherSpine[0].node).repeatForever(
                tween().by(4, {position: cc.v3(1150, 0)})
                    .call(() => {
                        this.otherSpine[0].node.scaleX = 1;
                    })
                    .by(4, {position: cc.v3(-1150, 0)})
                    .call(() => {
                        this.otherSpine[0].node.scaleX = -1;
                    })
            )
            .start();
    }

    runOption1(): void {
        let light = this.otherSprite[0].node;
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                tween(this.lupin.node).call(() => {
                        this.playSound(SOUND.ELECTROCUTE, false, 1.1)
                        let isTrue = true;
                        let isTrue1 = true;
                        this.otherSprite[1].node.active = true;
                        light.active = true;
                        light.scaleX = 2;
                        light.scaleY = 0.7;
                        light.position = cc.v3(-665, -480);
                        this.lupin.setAnimation(1, "emotion/excited", true);
                        this.lupin.setAnimation(0, "level_10/electric_bring", false);
                        this.lupin.addAnimation(0, "general/electric_die", true);
                        this.lupin.setStartListener(track => {
                            if (track.animation.name == "general/electric_die" && isTrue1)
                            {
                                isTrue1 = false;
                                this.lupin.node.position = cc.v3(this.lupin.node.x, this.lupin.node.y - 300);
                            }
                        })
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "general/electric_die" && isTrue)
                            {
                                isTrue = false;
                                tween(this.lupin.node).delay(2).call(() => {
                                    cc.Tween.stopAllByTarget(this.otherSpine[0].node);
                                        this.showFail(this.selected);
                                    }).start();
                            }
                        });
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        let light = this.otherSprite[0].node;
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                light.scale = 2;
                light.active = true;
                light.position = cc.v3(-1070, -205);
                this.lupin.setAnimation(0, "level_10/candle_bring", true);
                this.lupin.setAnimation(1, "emotion/excited", true);
                tween(this.lupin.node).by(4, {position: cc.v3(1000, 0)})
                    .call(() => {
                        this.otherSpine[2].node.active = true;
                        cc.Tween.stopAllByTarget(this.otherSpine[0].node);
                        this.otherSpine[0].node.position = cc.v3(-1250, 390);
                        this.otherSpine[0].node.scaleX = -1;
                        this.otherSpine[0].setAnimation(0, "police/general/walk", true);
                        this.otherSpine[1].setAnimation(0, "run_1", true);
                        this.playSound(SOUND.ALERT, false, 0)
                        this.playSound(SOUND.BARK, false, 1)
                        this.playSound(SOUND.BARK, false, 4)
                        tween(this.otherSpine[0].node).by(2, {position: cc.v3(1200, 0)})
                            .call(() => {

                                this.otherSpine[0].setAnimation(0, "detect", false);
                                this.otherSpine[1].setAnimation(0, "walk_smell", false);
                                this.otherSpine[1].setCompleteListener(track => {
                                    if (track.animation.name == "walk_smell")
                                    {
                                        this.showFail(this.selected);
                                    }
                                })
                            })
                            .start();
                    })
                    .by(2, {position: cc.v3(500, 0)})
                    .call(() => {
                        this.lupin.node.scaleX = -1;
                        this.otherSprite[0].node.active = false;
                        this.lupin.setAnimation(0, "general/back", false);
                        this.lupin.setAnimation(1, "emotion/fear_1", true);
                    })
                    .start();
                tween(this.otherSprite[0].node).by(4, {position: cc.v3(1000, 0)})
                    .by(2, {position: cc.v3(500, 0)})
                    .start();
                tween(this.camera2d[0]).by(5, {position: cc.v3(1400, 0)}).start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        let light = this.otherSprite[0].node;
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.lupin.setAnimation(1, "emotion/excited", true);
                this.lupin.setAnimation(0, "level_10/firely_bring", true);
                tween(this.lupin.node).by(6, {position: cc.v3(2600, 0)})
                    .call(() => {
                        light.active = false;
                        this.lupin.setAnimation(1, "emotion/excited", true);
                        this.lupin.setAnimation(0, "general/win_2.1", true);
                    })
                    .delay(1)
                    .call(() => {
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "level_9/door_creep", false);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "level_9/door_creep")
                            {
                                this.onPass();
                            }
                        });
                    })
                    .start();
                light.active = true;
                light.position = cc.v3(-1050, -205);
                tween(this.otherSprite[0].node).by(6, {position: cc.v3(2600, 0)}).start();
                tween(this.camera2d[0]).by(6, {position: cc.v3(2100, 0)}).start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }
}
