import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    FREEDOM,
    OWL,
    HESITATE,
    SPIN,
    THROW,
    SWOOSH,
    HIT,
    ALERT
}
@ccclass
export default class Level10_4 extends LevelBase {

    @property([cc.Prefab])
    manyBushs: cc.Prefab[] = [];

    @property(cc.Node)
    freedom: cc.Node = null;

    @property(cc.Node)
    backgroundEnd: cc.Node = null;

    bushTemps = []

    initStage(): void {
        super.initStage()

        this.setLupin(cc.v2(-205, -671), 'emotion/idle', 'general/hide2')
        this.lupin.timeScale = 1
        this.lupin.node.scale = 1.3
        this.lupin.node.zIndex = 2
        this.lupin.setCompleteListener(null)

        this.setOtherSpine(this.otherSpine[0], cc.v2(177, -124), 'police/level_10/stand_guard', '')
        this.otherSpine[0].timeScale = 1
        this.otherSpine[0].node.active = true

        this.otherSprite[0].node.zIndex = 0
        this.otherSprite[1].node.active = false
        // this.otherSprite[2].node.active = false
        // this.otherSprite[2].node.x = 1619

        this.backgroundEnd.active = false
        this.backgroundEnd.zIndex = 1
        this.backgroundEnd.x = 1619

        for (let bush of this.bushTemps) {
            bush.destroy()
        }

        this.bushTemps = []

        this.freedom.active = false
        this.freedom.zIndex = 3
        this.playSound(SOUND.OWL, false, 1)
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true)
        }).start()
    }

    many0Schedule() {
        const bush = cc.instantiate(this.manyBushs[0])
        bush.active = false
        this.bushTemps.push(bush)

        this.background.addChild(bush, 0, 'ManyBush')
        tween(bush)
            .tag(101)
            .to(0, {position: cc.v3(1380, 140)})
            .set({active: true, zIndex: 0})
            .by(2, {position: cc.v3(-2080)})
            .call(() => {
                bush.destroy()
            })
            .start()
    }

    many1Schedule () {
        const bush = cc.instantiate(this.manyBushs[1])
        bush.active = false
        this.bushTemps.push(bush)

        this.background.addChild(bush, 0, 'ManyBush')
        tween(bush)
            .tag(101)
            .to(0, {position: cc.v3(954, 70)})
            .set({active: true, zIndex: 1})
            .by(1.5, {position: cc.v3(-2580)})
            .call(() => {
                bush.destroy()
            })
            .start()
    }

    many2Schedule () {
        const bush = cc.instantiate(this.manyBushs[2])
        bush.active = false
        this.bushTemps.push(bush)

        this.background.addChild(bush, 0, 'ManyBush')
        tween(bush)
            .tag(101)
            .to(0, {position: cc.v3(1060, -490)})
            .set({active: true, zIndex: 3})
            .by(.7, {position: cc.v3(-3080)})
            .call(() => {
                bush.destroy()
            })
            .start()
    }

    showEnd() {
        EffectManager.hideScene((node) => {
            this.setLupin(cc.v2(-300, -300), null, 'general/run_1')
            this.lupin.clearTrack(0)
            this.lupin.node.zIndex = 2

            this.otherSpine.forEach((spine) => {
                // this.otherSprite[2].node.active = true
                this.backgroundEnd.active = true
                spine.node.active = false
            })

            this.many0Schedule()
            this.many1Schedule()
            this.many2Schedule()

            EffectManager.showUI(false);

            EffectManager.showScene()

            // tween(this.otherSprite[2].node)
            //     .by(9.5, {position: cc.v3(-3177)})
            //     .start()

            tween(this.backgroundEnd)
                .by(9.5, {position: cc.v3(-3177)})
                .start()

            this.schedule(this.many0Schedule, 2)
            this.schedule(this.many1Schedule, 1.5)
            this.schedule(this.many2Schedule, .7)
            cc.audioEngine.pauseMusic()
            this.playSound(SOUND.FREEDOM, false, 0)
            tween(this.lupin.node)
                .by(9.5, {position: cc.v3(450)})
                .call(() => {
                    this.lupin.timeScale = 0

                    cc.Tween.stopAllByTag(101)
                    cc.audioEngine.stopAllEffects()
                    cc.audioEngine.resumeMusic()
                    this.unschedule(this.many0Schedule)
                    this.unschedule(this.many2Schedule)
                    this.unschedule(this.many1Schedule)
                    cc.error("showSuccess");
                    this.showSuccess(this.selected);
                })
                .start()

            tween(this.freedom)
                .delay(1)
                .set({scale: 5, active: true, opacity: 0})
                .to(.5, {scale: 1, opacity: 255})
                .delay(5)
                .to(.5, {scale: 5, opacity: 0})
                .set({active: false})
                .start()

        }, this.node)
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_10/throw_rock') {
                this.lupin.setCompleteListener(null);
                this.setLupin(cc.v2(this.lupin.node.position), 'general/hide2', 'emotion/excited')
                this.otherSprite[1].node.active = true
                this.otherSpine[0].setAnimation(0, 'police/general/walk2', true)

                tween(this.otherSpine[0].node)
                    .by(2, {position: cc.v3(-580)})
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'police/level_10/stand_guard', true)
                    })
                    .delay(1)
                    .call(() => {
                        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/whistle')

                        tween(this.lupin.node)
                            .by(2, {position: cc.v3(1100)})
                            .call(() => {
                                cc.error("this.ShowEnd");
                                this.showEnd()
                            })
                            .start()
                    })
                    .start()
            }
        })

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.playSound(SOUND.THROW, false, 1.6)
                this.lupin.setAnimation(1, 'level_10/throw_rock', false)
            })
            .start()
    }

    runOption2(): void {
        EffectManager.hideScene((node) => {
            this.setLupin(cc.v2(-20, -225), 'emotion/fear_1', 'general/stand_nervous')
            this.lupin.clearTrack(1)
            this.lupin.node.scale = .8
            this.lupin.node.zIndex = 0

            this.otherSprite[0].node.zIndex = 1

            this.otherSpine[0].setAnimation(0, 'police/level_10/stand_guard', true)
            this.playSound(SOUND.HESITATE, false, 0.5)
            this.playSound(SOUND.ALERT, false, 3)
            this.lupin.setCompleteListener((track) => {
                if (track.animation.name === 'level_10/raise_stick') {
                    this.otherSpine[0].setAnimation(0, 'police/level_10/turn_head', true)

                    tween(this.lupin.node)
                        .delay(.3)
                        .call(() => {
                            this.setLupin(
                                cc.v2(this.lupin.node.position),
                                'emotion/fear_2',
                                'general/back',
                            )
                        })
                        .delay(2)
                        .call(() => {
                            this.lupin.timeScale = 0
                            this.otherSpine[0].timeScale = 0

                            this.showContinue()
                        })
                        .start()
                }
            })

            EffectManager.showScene()
            tween(this.lupin.node)
                .delay(1)
                .call(() => {
                    this.lupin.setAnimation(1, 'level_10/raise_stick', false)
                })
                .start()
        }, this.node)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_10/throw_bumerang') {
                this.lupin.setAnimation(1, 'general/get_hit', false)
                this.lupin.timeScale = .5
            }

            if (track.animation.name === 'general/get_hit') {
                this.otherSpine[0].timeScale = 0
                this.lupin.timeScale = 0

                this.showContinue()

            }
        })

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.playSound(SOUND.SWOOSH, false, 0.5)
                this.playSound(SOUND.SPIN, false, 1)
                this.scheduleOnce(()=>{
                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.HIT, false, 0)
                },2.6)
                this.lupin.setAnimation(1, 'level_10/throw_bumerang', false)
            })
            .start()
    }
}
