import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    SWOOSH,
    SLIP,
    ALERT,
    BREAK,
    FREEZE,
    HIT,
    SCREAM,
    THROW
  }
@ccclass
export default class Level7_3 extends LevelBase {

    onEnable(): void {
        super.onEnable();
    }

    onDisable(): void {
        super.onDisable();
    }

    /** otherSpine
     * 0 là police
     * 1 là đèn
     * 2 là khung ảnh
     * 3 là bình hoa
     */

    initStage(): void {
        super.initStage();
        this._gameManager.mainCamera.active = false;
        this.lupin.setMix("general/creep_high", "general/hide_under_table", 0.3);
        this.otherSpine[3].setAnimation(0, "level_7/vase_breaken", false);
        this.otherSpine[1].setAnimation(0, "level_7/lamb_break", false);
        this.otherSpine[1].node.position = cc.v3(70, 315);
        this.otherSpine[2].setAnimation(0, "level_7/picture_drop", false);
        this.setOtherSpine(this.otherSpine[0], cc.v2(-50, -200), "police/level_7/book_work", "police/general/sit_write");
        this.otherSpine[1].timeScale = 0;
        this.otherSpine[2].timeScale = 0;
        this.otherSpine[3].timeScale = 0;
        this.otherSpine[2].node.zIndex = 100;
        this.otherSpine[0].node.zIndex = 101;
        this.otherSprite[1].node.zIndex = 100;
        this.lupin.node.zIndex = cc.macro.MAX_ZINDEX;
        this.setLupin(cc.v2(-1000, -350), "emotion/excited", "general/creep_high");
        tween(this.lupin.node).by(4, {position: cc.v3(690, 0)})
                .call(() => {
                    this.lupin.setAnimation(1, "general/hide_under_table", true);
                    tween(this.node).delay(2).call(() => {
                        this.showOptionContainer(true);
                    }).start();
                })
                .start();
        this.otherSprite[0].node.active = false;
        this.otherSpine[0].node.scaleX = 0.8;
        this.camera2d[0].active = true;
        this.camera2d[1].active = true;
    }

    runOption1(): void {
        tween(this.lupin.node).delay(1)
        .call(() => {
            this.playSound(SOUND.BREAK, false, 1)
            this.lupin.setAnimation(1, "level_7/vase_break", false);
            tween(this.otherSpine[3].node).delay(1).call(() => {
                    this.otherSpine[3].timeScale = 1;
                }).start();
            this.otherSpine[3].setCompleteListener(trackEntry => {
                console.log("%c" + trackEntry.animation.name, "color:blue");
                if (trackEntry.animation.name == "level_7/vase_breaken")
                {
                    this.playSound(SOUND.SCREAM, false, 0.1)
                    this.otherSpine[0].setAnimation(1, "police/level_7/vase_heart_break", false);
                    this.otherSpine[0].setCompleteListener(track => {
                        if (track.animation.name == "police/level_7/vase_heart_break")
                        {
                            this.lupin.setAnimation(0, "emotion/fear_1", true);
                            this.lupin.setAnimation(1, "general/run", true);
                            tween(this.node).delay(1).call(() => {this.showSuccess(this.selected);}).start();
                            tween(this.lupin.node).by(1.8, {position: cc.v3(650, 30)})
                                .call(() => {
                                    this.lupin.timeScale = 0;
                                })
                                .start();
                        }
                    })
                }
            })
        })
        .start();
    }

    runOption2(): void {
        tween(this.lupin.node).delay(1)
            .call(() => {
                this.playSound(SOUND.THROW, false, 1.5)
                this.playSound(SOUND.HIT, false, 2)
                this.playSound(SOUND.FREEZE, false, 2.5)

                this.lupin.setAnimation(1, "level_7/shoot_slice", false);
                this.lupin.setCompleteListener(trackEntry => {
                    console.log("%c" + trackEntry.animation.name, "color:blue");
                    if (trackEntry.animation.name == "level_7/shoot_slice")
                    {
                        this.otherSpine[1].timeScale = 1;
                        tween(this.otherSpine[1].node).by(0.3, {position: cc.v3(0, -615)}, {easing: "cubicIn"})
                            .call(() => {
                                this.otherSprite[0].node.active = true;
                                this.otherSprite[0].node.zIndex = 99;
                                this.otherSpine[0].node.zIndex = 98;
                                this.otherSpine[0].setAnimation(1, "police/level_7/stand_angry", false);
                                this.otherSpine[0].setAnimation(0, "police/level_7/stand_angry", false);
                                this.lupin.setAnimation(0, "emotion/fear_1", false);
                                this.lupin.setAnimation(1, "general/back", false);
                                tween(this.node).delay(1)
                                    .call(() => {
                                        this.lupin.timeScale = 0;
                                        this.otherSpine.forEach(spine => {
                                            spine.timeScale = 0;
                                        })
                                        tween(this.node).delay(1).call(() => {
                                            this.lupin.timeScale = 0;
                                            this.otherSpine.forEach(spine => {
                                                spine.timeScale = 0;
                                            })
                                            tween(this.node).call(() => {
                                                this.showContinue();
                                            })
                                            .start();
                                        })
                                        .start();;
                                    })
                                    .start();
                            })
                            .start();
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        let isTrue = true;
        this.lupin.setMix("general/hide_under_table", "general/run", 0.3);
        this.lupin.setMix("general/run", "general/fall_sml", 0.3);
        tween(this.lupin.node).delay(1)
        .call(() => {
            this.playSound(SOUND.SWOOSH, false, 0.5)
            this.lupin.setAnimation(1, "level_7/push_picture", false);
            this.lupin.addAnimation(1, "general/hide_under_table", false);
            tween(this.otherSpine[2].node).delay(0.5).call(() => {
                    this.otherSpine[2].timeScale = 1;
                    this.otherSprite[0].node.active = true;
                    this.otherSprite[0].node.zIndex = 102;
                }).start();
            this.lupin.setCompleteListener(trackEntry => {
                console.log("%c" + trackEntry.animation.name, "color:blue");
                if (trackEntry.animation.name == "level_7/push_picture" && isTrue)
                {
                    isTrue = false;
                    this.lupin.setAnimation(0, "emotion/excited", true);
                    this.otherSpine[0].setAnimation(1, "police/level_7/take_picture", false);
                    this.otherSpine[0].setCompleteListener(track => {
                        if (track.animation.name == "police/level_7/take_picture")
                        {
                            this.lupin.setAnimation(1, "general/run", true);
                            this.lupin.setAnimation(0, "emotion/fear_1", true);
                            tween(this.lupin.node).by(0.8, {position: cc.v3(410, 0)})
                                .call(() => {
                                    this.lupin.setAnimation(1, "general/fall_sml", false);
                                    this.playSound(SOUND.SLIP, false, 0)
                                })
                                .delay(1)
                                .call(() => {
                                    this.playSound(SOUND.ALERT, false, 0)
                                    this.otherSpine[0].setAnimation(1, "detect", false);
                                    this.otherSpine[0].setAnimation(0, "detect", false);
                                    this.otherSpine[0].node.scaleX = -0.8;
                                })
                                .delay(0.5)
                                .call(() => {
                                    this.lupin.timeScale = 0;
                                    tween(this.node).delay(1).call(() => {
                                        this.lupin.timeScale = 0;
                                        this.otherSpine.forEach(spine => {
                                            spine.timeScale = 0;
                                        })
                                        tween(this.node).call(() => {
                                            this.showContinue();
                                        })
                                        .start();
                                    })
                                    .start();;
                                })
                                .start();
                        }
                    })
                }
            })
        })
        .start();
    }
}
