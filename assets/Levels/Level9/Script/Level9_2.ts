import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND{
    BLEH_BLEH,
    BANG,
    FIRE,
    LIGHTER,
    SCREAM,
    SPIDER
}

@ccclass
export default class Level7_1 extends LevelBase {

    spidersIndex = [1, 2, 3, 4, 5]
    firesIndex = [6, 7, 8, 9, 10, 11, 12]

    initStage() {
        super.initStage();

        this._gameManager.mainCamera.active = false;

        this.camera2d[0].position = cc.v3(0, 0);
        this.camera2d[0].active = true;
        this.camera2d[1].active = true;

        this.setLupin(cc.v2(-379, -510), 'emotion/fear_2', 'general/stand_nervous')
        this.lupin.timeScale = 1
        this.lupin.setCompleteListener(null)

        this.otherSprite.forEach((sprite) => {
            sprite.node.active = true
        })

        this.setOtherSpine(this.otherSpine[0], cc.v2(288, -603), 'spider_creep_angry', '')
        this.otherSpine[0].timeScale = 1
        this.otherSpine[0].node.scaleX = 1

        this.spidersIndex.forEach((sIndex) => {
            const spine = this.otherSpine[sIndex]
            spine.setAnimation(0, 'spider_creep_angry', true)
            spine.timeScale = 1
        })

        const spiderArmy = this.background.getChildByName('SpiderArmy')
        spiderArmy.position = cc.v3(0, 0)

        for (let index of this.firesIndex) {
            const spine = this.otherSpine[index]
            spine.node.active = false
            spine.timeScale = 1
        }

        tween(this.node)
            .delay(2)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1() {
        this.setLupin(cc.v2(this.lupin.node.position), 'emotion/excited', 'general/stand_thinking')
        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.clearTrack(1)
                this.playSound(SOUND.BLEH_BLEH, false, 0.2)
                this.lupin.setAnimation(0, 'general/tougue_show', true)
            })
            .delay(1)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'spider_creep_nervous', true)
                tween(this.otherSpine[0].node)
                    .flipX()
                    .by(2.5, {position: cc.v3(400)})
                    .delay(.5)
                    .call(() => {
                        this.setLupin(
                            cc.v2(this.lupin.node.position),
                            'emotion/happy_1',
                            'general/win_2.1')
                    })
                    .delay(2)
                    .call(() => {
                        this.onPass()
                    })
                    .start()
            })
            .start()
    }

    runOption2() {
        EffectManager.hideScene((node) => {
            this.setLupin(cc.v2(-175, -578), 'emotion/excited', 'general/stand_thinking')

            EffectManager.showScene()
            tween(this.lupin.node)
                .delay(1)
                .call(() => {
                    this.playSound(SOUND.BANG, true, 0)
                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'emotion/angry',
                        'general/stick_fight',
                    )
                })
                .delay(1)
                .call(() => {
                    this.otherSprite[0].node.active = false
                    this.otherSprite[1].node.active = false

                    this.otherSpine[0].setAnimation(0, 'spider_creep_nervous', true)
                    tween(this.otherSpine[0].node)
                        .flipX()
                        .by(2.5, {position: cc.v3(387)})
                        .delay(1)
                        .flipX()
                        .call(() => {
                            this.scheduleOnce(()=>{
                                cc.audioEngine.stopAllEffects()
                                this.playSound(SOUND.SPIDER, false, 0)
                                this.playSound(SOUND.SCREAM, false, 2)

                            },1)
                            this.otherSpine[0].setAnimation(0, 'spider_creep_angry', true)
                        })
                        .call(() => {
                            const spiderArmy = this.background.getChildByName('SpiderArmy')
                            tween(spiderArmy)
                                .delay(.8)
                                .by(3, {position: cc.v3(-960)})
                                .call(() => {
                                    this.lupin.timeScale = 0
                                    this.otherSpine[0].timeScale = 0

                                    this.spidersIndex.forEach((sIndex) => {
                                        const spine = this.otherSpine[sIndex]
                                        spine.timeScale = 0
                                    })

                                    this.showContinue()
                                })
                                .start()
                        })
                        .by(2.5, {position: cc.v3(-780)})
                        .start()
                })
                .delay(1)
                .call(() => {
                    this.otherSprite[2].node.active = false
                })
                .delay(3.5)
                .call(() => {
                    this.lupin.setAnimation(0, 'emotion/fear_2', true)
                    this.lupin.setAnimation(1, 'general/back', false)
                })
                .delay(4)
                .start()

        }, this.node)
    }

    runOption3() {
        this.lupin.setMix('level_9/fire_burn', 'general/back', .3)

        this.setLupin(cc.v2(this.lupin.node.position), 'emotion/excited', 'general/stand_thinking')
        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(1, 'general/walk', true)
            })
            .by(1, {position: cc.v3(120, -100)})
            .call(() => {
                this.playSound(SOUND.LIGHTER, false,0.2)
                this.playSound(SOUND.FIRE, false,0.5)
                this.playSound(SOUND.SCREAM, false,5)
                this.lupin.setAnimation(1, 'level_9/fire_burn', true)
            })
            .delay(1)
            .call(() => {
                this.firesIndex.forEach((fIndex, index) => {
                    const fire = this.otherSpine[fIndex]
                    let delay = index > 4 ? 4 : (index + 1) * .5
                    tween(fire.node).delay(delay).set({active: true}).start()
                })
            })
            .delay(4)
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'emotion/fear_2', 'general/back')
            })
            .delay(2)
            .call(() => {
               this.lupin.timeScale = 0
               this.firesIndex.forEach((fIndex) => {
                   const fire = this.otherSpine[fIndex]
                   fire.timeScale = 0
               })

               this.showContinue()
            })
            .start()
    }
}