import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    SNEAK,
    WHISTLE
  }
@ccclass
export default class Level6Option2_1 extends LevelBase {

    onEnable(): void {
        this.lupin.setSkin('Lupin')
        tween(this.shadow).to(0.3, {opacity: 0}).start();
        this.runAction();
        this.lupin.timeScale = 1;
        this.otherSpine.forEach(spine => {
            spine.timeScale = 1;
        });
        this.selected = 1;
    }

    runAction(): void {
        this.lupin.setMix("general/walk_slow", "general/stand_thinking", 0.5);
        this.setLupin(cc.v2(-680, -355), "emotion/worry", "general/walk_slow");
        this.playSound(SOUND.SNEAK, false, 0.3)
        tween(this.lupin.node).by(5, {position: cc.v3(480, 0)})
                    .call(() => {
                        this.setLupin(cc.v2(-200, -355), "emotion/excited", "general/stand_thinking");
                    })
                    .delay(2)
                    .parallel(
                        tween().by(5, {position: cc.v3(945, 0)}).call(() => {
                                    this.onPass();
                                }),
                        tween().call(() => {
                            this.playSound(SOUND.WHISTLE, false, 0)
                            this.setLupin(cc.v2(-200, -355), "emotion/whistle", "general/walk");
                        })
                    )
                    .start();
    }
}