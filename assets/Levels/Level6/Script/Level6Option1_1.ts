import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    PASTOR,
    ALERT
  }

@ccclass
export default class Level6Option1_1 extends LevelBase {

    onEnable(): void {
        this.lupin.setSkin('Lupin')
        tween(this.shadow).to(0.3, {opacity: 0}).start();
        this.runAction();
        this.lupin.timeScale = 1;
        this.otherSpine.forEach(spine => {
            spine.timeScale = 1;
        });
        this.selected = 0;
    }

    runAction(): void {
        var isTrue = true;
        tween(this.node).call(() => {
                this.setLupin(cc.v2(-680, -355), "emotion/worry", "general/walk_slow");
                tween(this.lupin.node).by(3, {position: cc.v3(300, 0)})
                            .call(() => {
                                this.otherSpine[0].setAnimation(0, "pastor/level_6/shoot", true);
                                this.scheduleOnce(() => {
                                    this.otherSpine[1].setAnimation(0, "police/general/gun_raise", true);
                                }, 1);

                            })
                            .by(1, {position: cc.v3(50, 0)})
                            .call(() => {
                                this.setLupin(cc.v2(this.lupin.node.position), "emotion/fear_1", "general/back");
                                this.lupin.setCompleteListener(trackEntry => {
                                    if (trackEntry.animation.name == "emotion/fear_1" && isTrue)
                                    {
                                        isTrue = false;
                                        tween(this.node).call(() => {
                                                EffectManager.showX(this.selected, this.getLineCurrent());
                                            })
                                            .delay(1)
                                            .call(function() {
                                                this.lupin.timeScale = 0;
                                                this.otherSpine.forEach(police => {
                                                    police.timeScale = 0;
                                                });
                                                EffectManager.effectFail();
                                                EffectManager.showUI(false);
                                            }.bind(this))
                                            .delay(1)
                                            .call(() => {
                                                this.showFail(this.selected);
                                            })
                                            .start();
                                    }
                                });
                            })
                            .start();
                this.playSound(SOUND.PASTOR, false, 0)
                this.playSound(SOUND.ALERT, false, 4)

                this.otherSpine[0].setAnimation(0, "pastor/level_6/teach", true);
                this.otherSpine[1].setAnimation(0, "police/level_6/learn", true);
            })
            .delay(0.5)
            .call(() => {
                this.otherSpine[2].setAnimation(0, "police/level_6/learn", true);
            })
            .delay(0.5)
            .call(() => {
                this.otherSpine[3].setAnimation(0, "police/level_6/learn", true);
            })
            .start();
    }
}
