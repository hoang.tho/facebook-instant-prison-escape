import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";


const {ccclass, property} = cc._decorator;

enum SOUND {
    BARK,
    BARK_RUN,
    GROWL,
    THROW,
    WHISLE,
    ALERT
  }
var tween = cc.tween;
@ccclass
export default class Level2 extends LevelBase {

    private lupinSoundId: number;

    onLoad(): void {
        super.onLoad();
    }

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage() : void {
        super.initStage();
        super.setLupin(cc.v2(-240, -510), "emotion/idle", "general/stand_thinking");
        super.setOtherSpine(this.otherSpine[1], cc.v2(700, -550), "run", null);
        super.setOtherSpine(this.otherSpine[0], cc.v2(750, -510), null, null);
        this.background.position = cc.v3(540, -30);
        this.lupin.node.active = true;
        this.otherSpine[0].node.active = true;
        this.otherSpine[1].node.active = true;
        this.playSound(SOUND.GROWL, false, 2)
        tween(this.otherSpine[1].node).to(2.5, {position: cc.v3(270, -510)})
                                    .call(() => {
                                        this.lupin.setAnimation(0, "emotion/idle", true);
                                        this.lupin.setAnimation(1, "emotion/worry", true);
                                        this.otherSpine[1].setAnimation(0, "stand_angry", true);
                                        this.showOptionContainer(true);
                                    })
                                    .start();
        this.showOptionContainer(false);
        this
    }

    runOption1(): void {
        this.lupin.setMix("level_2/lv2_stg1_bone", "emotion/idle", 0.3);
        tween(this.node).delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "level_2/lv2_stg1_bone", false);
                this.lupin.setAnimation(1, "emotion/sinister", true);
                this.playSound(SOUND.THROW, false, 2.8)
                this.lupin.setCompleteListener(trackEntry => {
                    if (trackEntry.animation.name == "level_2/lv2_stg1_bone")
                    {
                        this.otherSpine[1].setAnimation(0, "run_1", true);
                        this.playSound(SOUND.BARK_RUN, false, 0)
                        this.playSound(SOUND.BARK_RUN, false, 1)
                        this.playSound(SOUND.WHISLE, false, 2)
                        this.lupin.addAnimation(0, "emotion/idle", true);
                        tween(this.lupin).delay(2)
                            .call(() => {
                                this.lupin.setAnimation(1, "emotion/whistle", true);
                            })
                            .start();
                        tween(this.otherSpine[1].node).to(1.5, {position: cc.v3(-750, -550)}).start();
                        tween(this.node).call(() => {
                        })
                        .delay(5)
                        .call(() => {
                            this.lupin.setAnimation(0, "general/walk", true);
                            tween(this.background).to(4, {position: cc.v3(-525, -30)}).start();
                            tween(this.lupin.node).to(4, {position: cc.v3(70, -510)})
                                .call(() => {
                                    this.lupin.setAnimation(0, "general/walk_upstairs", true);
                                })
                                .by(1, {position: cc.v3(100, 75)})
                                .call(() => {
                                    this.showSuccess(this.selected);
                                })
                                .by(1, {position: cc.v3(100, 75)})
                                .start();
                        })
                        .start();
                    }
                })
            })
            .start();
    }

    runOption2(): void {
        tween(this.node).delay(1).call(() => {
            this.lupin.setAnimation(0, "level_2/lv2_stg1_meat", false);
            this.lupin.setAnimation(1, "emotion/fear_1", true);
            this.playSound(SOUND.THROW, false, 1.3)
            this.lupin.setCompleteListener(trackEntry => {
                if (trackEntry.animation.name == "level_2/lv2_stg1_meat")
                {
                    this.playSound(SOUND.GROWL, false, 0)
                    this.lupin.setAnimation(0, "general/stand_nervous", true);
                    this.lupin.setAnimation(1, "emotion/excited", true);
                    this.otherSpine[1].setAnimation(0, "catch_eat", false);
                }
            });
            this.otherSpine[1].setCompleteListener(trackEntry => {
                if (trackEntry.animation.name == "catch_eat")
                {
                    this.lupin.setAnimation(1, "emotion/fear_2", false);
                    this.lupin.setAnimation(0, "general/walk", false);
                    this.otherSpine[1].setAnimation(0, "stand_angry", true);
                    tween(this.lupin.node)
                        .by(0.1, {position: cc.v3(-40, 0)})
                        .call(() => {
                            this.lupin.setAnimation(0, "general/stand_nervous", true);
                        })
                        .delay(1)
                        .call(() => {
                            this.showFail(this.selected);
                        })
                        .start();
                }
            });
        }).start();
    }

    runOption3(): void {
        tween(this.node).delay(1).call(() => {
            this.playSound(SOUND.THROW, false, 2.5)
            this.lupin.setAnimation(0, "level_2/lv2_stg1_potato", false);
            this.lupin.setCompleteListener(trackEntry => {
                if (trackEntry.animation.name == "level_2/lv2_stg1_potato")
                {
                    this.playSound(SOUND.BARK, false, 0.3)
                    this.playSound(SOUND.BARK, false, 1.3)
                    this.lupin.setAnimation(1, "emotion/fear_2", true);
                    this.lupin.setAnimation(0, "general/stand_nervous", true);
                    this.otherSpine[1].setAnimation(0, "shout", true);
                    //police/general/gun_raise
                    //police/general
                    this.playSound(SOUND.ALERT, false, 2.3)
                    tween(this.otherSpine[0].node).delay(2)
                            .call(() => {
                                this.otherSpine[0].setAnimation(0, "police/general/run", true);
                                tween(this.otherSpine[0].node).to(1, {position: cc.v3(385, -510)})
                                    .call(() => {
                                        this.otherSpine[0].setAnimation(0, "police/general/gun_raise", false);
                                        this.otherSpine[0].setCompleteListener(trackEntry => {
                                            if (trackEntry.animation.name == "police/general/gun_raise")
                                            {
                                                this.showFail(this.selected);
                                            }
                                        });
                                    }).start();
                            }).start();
                }
            })
        }).start();
    }
}
