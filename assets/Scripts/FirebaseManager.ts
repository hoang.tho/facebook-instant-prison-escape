import FBInstantManager from './FBInstantManager'

const {ccclass, property} = cc._decorator;

const REFS = {
    'players': 'players',
    'config': 'config',
}

const CONFIG_ID = 'configId'

const lazyfunction = function(target, propertyName, descriptor) {
    const instance = target.constructor.getInstance()
    const originalMethod = descriptor.value

    descriptor.value = function (...args) {
        if (!instance._firebase) {
            instance._lazyCalls.push({
                func: descriptor.value,
                args: arguments,
            })

            return
        }

        return originalMethod.apply(instance, args)
    }
}

@ccclass
export default class FirebaseManager {

    private _firebase = null
    private _db = null
    private _lazyCalls = []

    constructor() {}

    private static _instance = null

    public static getInstance(): FirebaseManager {
        if (FirebaseManager._instance)
            return FirebaseManager._instance;
        else {
            FirebaseManager._instance = new FirebaseManager();
            return FirebaseManager._instance;
        }
    }

    public initFirebase() {
        (() => {
            let loadedScript = 0

            // const metaTag = document.createElement('meta')
            // metaTag.httpEquiv = 'Content-Security-Policy'
            // metaTag.content = "script-src 'self' *.firebasedatabase.app 'unsafe-inline' 'unsafe-eval' *.facebook.com connect.facebook.net cdn.mixpnl.com *.google-analytics.com web.localytics.com *.googletagmanager.com blob: *.cloudfront.net *.amazonaws.com *.googleapis.com *.firebaseapp.com *.firebaseio.com *.8686c.com *.cncovs.com *.aliyun.com *.aliyuncs.com *.wsdvs.com *.console.re *.kunlunar.com *.layabox.com *.windows.net *.msecnd.net *.anysdk.com cdn.trackjs.com cdn.firebase.com *.kochava.com *.akamaized.net *.cocos.com *.hinet.net *.playfab.com code.createjs.com *.zdassets.com websdk.appsflyer.com cdnjs.cloudflare.com www.gstatic.com *.azureedge.net *.firebasedatabase.app"
            // document.head.appendChild(metaTag)
            // console.log('append meta', metaTag.content)
            // <script src="https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js"></script>
            // <script src="https://www.gstatic.com/firebasejs/8.6.1/firebase-database.js"></script>
            const onload = e => {
                loadedScript++
                console.log(loadedScript)
                if (loadedScript < 2) {
                    
                    script2.onload = onload
                    script2.src = 'https://www.gstatic.com/firebasejs/8.6.1/firebase-firestore.js'
                    document.body.appendChild(script2)
                    return
                }

                // Your web app's Firebase configuration
                // For Firebase JS SDK v7.20.0 and later, measurementId is optional
                const firebaseConfig = {
                    /*
                    * product config
                    * */

                    apiKey: "AIzaSyAtgQV4Y4C421i5yXu-LPnBLduNq_k8180",
                    authDomain: "prison-escape-intants-facebook.firebaseapp.com",
                    projectId: "prison-escape-intants-facebook",
                    storageBucket: "prison-escape-intants-facebook.appspot.com",
                    messagingSenderId: "665662688817",
                    appId: "1:665662688817:web:ef8b820f7b2b73b9624f10",
                    measurementId: "G-XP2FTDN4XW"

                    /*
                    * test config
                    * */

                    // apiKey: "AIzaSyDi8n9v1iX7VzUXmv1rKwNS-AJfBN2zXQ8",
                    // authDomain: "sampletest-880f0.firebaseapp.com",
                    // databaseURL: "https://sampletest-880f0-default-rtdb.asia-southeast1.firebasedatabase.app",
                    // projectId: "sampletest-880f0",
                    // storageBucket: "sampletest-880f0.appspot.com",
                    // messagingSenderId: "957162998019",
                    // appId: "1:957162998019:web:8f0aa803cf131316140136",
                    // measurementId: "G-2G3G4V2FP1"
                };
            
                firebase.initializeApp(firebaseConfig);

                this._firebase = firebase
                this._db = firebase.firestore()
                window.firebase = null

                this._lazyCalls.forEach(lazy => lazy.func(...lazy.args))
            }
        
            const script1 = document.createElement('script')
            const script2 = document.createElement('script')
            script1.onload = onload
            script1.src = 'https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js'
        
            document.body.appendChild(script1)
        })()
    }

    public getPlayerRef(ref) {
        const playerId = FBInstantManager.getInstance().getPlayerId()
        return ref + '/' + playerId
    }

    public setData(ref, index, data) {
        try {
            this._db.collection(ref).doc(index).set(data)
                .then(() => {
                    console.log("Document written");
                })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
        } catch (err) {
            console.error('save firebase err: ', err)
        }
    }

    public getData(ref, index, cb) {
        this._db.collection(ref).doc(index).get().then((doc) => {
            if (doc.exists) {
                const data = doc.data()
                cb(null, data)
            } else {
                console.log('no doc', ref, index)
                cb(null, null)
            }
        }).catch((error) => {
            cb(error)
        });
    }

    public setPlayerData(data) {
        const playerId = FBInstantManager.getInstance().getPlayerId()
        if (playerId === 'localId') return

        this.setData(REFS.players, playerId, data)
    }

    public getPlayerData(cb) {
        const playerId = FBInstantManager.getInstance().getPlayerId()
        if (playerId === 'localId') return cb(null, null)
        this.getData(REFS.players, playerId, cb)
    }

    public getFriendData(friendId, cb) {
        const playerId = FBInstantManager.getInstance().getPlayerId()
        if (playerId === 'localId') return cb(null, null)
        this.getData(REFS.players, friendId, cb)
    }

    public getLeaderboard(cb) {
        const leaderboard = []

        const getFriends = (players, index, cb) => {
            const player = players[index]

            this.getFriendData(player.id, (err, data) => {
                if (err) {
                    data = {
                        unlockedLevel: -1
                    }
                }

                const friendData = {
                    ...data,
                    ...player,
                }

                leaderboard.push(friendData)
                index++

                if (players[index]) {
                    return getFriends(players, index, cb)
                }

                cb(leaderboard)
            })
        }

        FBInstantManager.getInstance().getConnectedPlayers(players => {
            getFriends(players, 0, cb)
        })
    }

    @lazyfunction
    getConfig(cb) {
        this._db.collection(REFS.config).doc(CONFIG_ID).get().then(doc => {
            if (doc.exists) {
                cb(null, doc.data())
            } else {
                cb(null, null)
            }
        }).catch(error => {
            cb(error, null)
        })
    }

}
